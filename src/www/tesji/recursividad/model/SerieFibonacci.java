/* Nombre: Hernandez Lopez Raul @Neo
 * Correo: freeenergy1975@gmail.com
 * Fecha:  15 de Nobiembre del 2021
 * */

package www.tesji.recursividad.model;

public class SerieFibonacci {

	public static void main(String[] args) {
		//Creaci�n del objeto
		SerieFibonacci objFibonacci = new SerieFibonacci();
		//LLamada a los metodos.
		System.out.print("\nLa serie fibonacci por recursividad de 4 es :" + objFibonacci.fibonacciRecursivo(4)
				+ "\nMientas que por ciclo es :" + objFibonacci.fibonacciCiclo(4));

	}
	
	public int fibonacciRecursivo(int numero) {
		//la serie fibonacci del 1 y del 2 es 1.
		if ((numero == 1) ||(numero == 2)) {
			return 1;
		}else {
			//La serie fibonacci del numero ingresado es igual a la suma de las dos series anterioeres 
			return fibonacciRecursivo(numero-1) + fibonacciRecursivo(numero -2);
		}
	}
	
	public int fibonacciCiclo(int numero) {
		//Declaracion he inicializacion de los valores que emplearemos..
		int fibonacci = 0, primer = 1, segundo = 0;
		/*mientras que numero sea mayor a cero el intercambio de valores se llevara a cabo y el numero 
		  de veces que esto ocurra esta determinado por el valor numero*/
		while(numero > 0) {
			fibonacci = primer + segundo;
			primer = segundo;
			segundo = fibonacci;
			numero --;
		}
		return fibonacci;//retorno del valor final.
	}

}
